//
//  NavigationController.m
//  Disrupt
//
//  Created by Izad Che Muda on 19/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "NavigationController.h"
#import "Constants.h"

@implementation NavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationBar.titleTextAttributes = @{
        NSForegroundColorAttributeName: [UIColor whiteColor],
        NSFontAttributeName: [UIFont systemFontOfSize:14 weight:UIFontWeightLight]
    };
    
    self.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationBar.translucent = NO;
    self.navigationBar.barTintColor = kTintColor;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
