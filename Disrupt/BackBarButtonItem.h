//
//  BackBarButtonItem.h
//  Disrupt
//
//  Created by Izad Che Muda on 20/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BackBarButtonItem : UIBarButtonItem

@property (nonatomic, strong) UIViewController *viewController;

- (id)initWithViewController:(UIViewController *)viewController;
- (void)buttonTapped:(UIBarButtonItem *)sender;

+ (id)backBarButtonItemWithViewController:(UIViewController *)viewController;

@end
