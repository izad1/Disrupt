//
//  StoryTableViewCell.h
//  Disrupt
//
//  Created by Izad Che Muda on 19/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProxyButton.h"
#import "Story.h"

@interface StoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *byLabel;
@property (weak, nonatomic) IBOutlet UILabel *domainLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *agoLabel;
@property (weak, nonatomic) IBOutlet ProxyButton *commentButton;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UIView *commentButtonContainerView;

- (void)configureWithStory:(Story *)story;

@end
