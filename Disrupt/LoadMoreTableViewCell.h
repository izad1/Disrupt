//
//  LoadMoreTableViewCell.h
//  Disrupt
//
//  Created by Izad Che Muda on 21/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface LoadMoreTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (nonatomic) BOOL enabled;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end
