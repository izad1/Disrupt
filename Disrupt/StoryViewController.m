//
//  StoryViewController.m
//  Disrupt
//
//  Created by Izad Che Muda on 20/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "StoryViewController.h"
#import "BackBarButtonItem.h"
#import "Constants.h"

@implementation StoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationItem.leftBarButtonItem = [BackBarButtonItem backBarButtonItemWithViewController:self];
    
    UIBarButtonItem *commentButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Comment"] style:UIBarButtonItemStylePlain target:self action:nil];
    UIBarButtonItem *actionsButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Actions"] style:UIBarButtonItemStylePlain target:self action:nil];
    self.navigationItem.rightBarButtonItems = @[commentButton, actionsButton];
    
    self.webView = [[WKWebView alloc] initWithFrame:kContentFrame];
    self.webView.allowsBackForwardNavigationGestures = YES;
    [self.webView addObserver:self
                   forKeyPath:@"estimatedProgress"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    [self.webView loadRequest:[NSURLRequest requestWithURL:self.story.URL]];
    [self.view addSubview:self.webView];
    
    self.progressView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 4)];
    self.progressView.backgroundColor = [UIColor colorWithHexString:@"#8EA604"];
    [self.view addSubview:self.progressView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (self.navigationController.viewControllers.count > 1) {
        return YES;
    }
    
    return NO;
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        if (self.webView.estimatedProgress < 1) {
            self.progressView.hidden = NO;
            self.progressView.alpha = 1;
        }
        
        CGFloat width = self.webView.estimatedProgress * kScreenWidth;
        self.progressView.frame = CGRectMake(self.progressView.frame.origin.x, self.progressView.frame.origin.y, width, self.progressView.frame.size.height);
        
        if (self.webView.estimatedProgress == 1) {
            [UIView animateWithDuration:0.5 animations:^{
                self.progressView.alpha = 0;
            } completion:^(BOOL finished) {
                self.progressView.hidden = YES;
            }];
        }
    }
}

- (void)webView:(WKWebView *)webView didCommitNavigation:(null_unspecified WKNavigation *)navigation {
    NSLog(@"commit");
}

@end
