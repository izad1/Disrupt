//
//  Story.h
//  Disrupt
//
//  Created by Izad Che Muda on 21/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Item.h"

@interface Story : Item

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *URLString;
@property (nonatomic, readonly) NSURL *URL;
@property (nonatomic, readonly) NSInteger commentCount;
@property (nonatomic, readonly) NSInteger score;
@property (nonatomic) BOOL isStory;
@property (nonatomic) BOOL isAsk;
@property (nonatomic) BOOL isJob;
@property (nonatomic) BOOL isPoll;

@end
