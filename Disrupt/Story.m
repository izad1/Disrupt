//
//  Story.m
//  Disrupt
//
//  Created by Izad Che Muda on 21/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "Story.h"

@implementation Story


- (NSString *)description {
    return  self.title;
}


- (NSString *)title {
    return [self.dictionary objectForKey:@"title"];
}


- (NSString *)URLString {
    if (![self.type isEqualToString:@"story"]) {
        return nil;
    }
    
    NSString *URLString = [self.dictionary objectForKey:@"url"];
    
    if (!URLString.length) {
        return nil;
    }
    
    return URLString;
}


- (NSURL *)URL {
    if (!self.URLString) {
        return nil;
    }
    
    return [NSURL URLWithString:self.URLString];
}


- (NSInteger)commentCount {
    NSNumber *descendants = [self.dictionary objectForKey:@"descendants"];
    return descendants.integerValue;
}


- (NSInteger)score {
    NSNumber *score = [self.dictionary objectForKey:@"score"];;
    return score.integerValue;
}


- (BOOL)isStory {
    return [self.type isEqualToString:@"story"] && self.URL;
}


- (BOOL)isAsk {
    return [self.type isEqualToString:@"story"] && !self.URL;
}


- (BOOL)isJob {
    return [self.type isEqualToString:@"job"];
}


- (BOOL)isPoll {
    return [self.type isEqualToString:@"poll"];
}

@end
