//
//  StoryTableViewCell.m
//  Disrupt
//
//  Created by Izad Che Muda on 19/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "StoryTableViewCell.h"
#import "NSDate+DateTools.h"
#import "HexColors.h"


@implementation StoryTableViewCell

- (void)awakeFromNib {
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    UIView *view = [UIView new];
    view.backgroundColor = [UIColor colorWithHexString:@"#F6F6EF"];
    
    self.selectedBackgroundView = view;
}


- (void)configureWithStory:(Story *)story {
    self.titleLabel.text = story.title;
    self.byLabel.text = story.by;
    
    if (story.isStory) {
        NSURLComponents *urlComponents = [NSURLComponents componentsWithString:story.URLString];
        NSArray *stringComponents = [urlComponents.host componentsSeparatedByString:@"."];
        NSInteger last = stringComponents.count - 1;
        self.domainLabel.text = [NSString stringWithFormat:@"%@.%@", [stringComponents objectAtIndex:last - 1], [stringComponents objectAtIndex:last]];
    }
    else if (story.isAsk) {
        self.domainLabel.text = @"Ask HN";
    }
    else if (story.isJob) {
        self.domainLabel.text = @"Job";
    }
    else if (story.isPoll) {
        self.domainLabel.text = @"Poll";
    }
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:story.time];

    NSString *ago = date.timeAgoSinceNow;
    ago = [ago stringByReplacingOccurrencesOfString:@"Yesterday" withString:@"1d"];
    ago = [ago stringByReplacingOccurrencesOfString:@" days ago" withString:@"d"];
    ago = [ago stringByReplacingOccurrencesOfString:@"1 day ago" withString:@"1d"];
    ago = [ago stringByReplacingOccurrencesOfString:@" hours ago" withString:@"h"];
    ago = [ago stringByReplacingOccurrencesOfString:@"An hour ago" withString:@"1h"];
    ago = [ago stringByReplacingOccurrencesOfString:@" minutes ago" withString:@"m"];
    ago = [ago stringByReplacingOccurrencesOfString:@"A minute ago" withString:@"1m"];
    self.agoLabel.text = ago;
    
    self.commentLabel.text = [NSString stringWithFormat:@"%ld", story.commentCount];
    self.scoreLabel.text = [NSString stringWithFormat:@"%ld", story.score];
}

@end
