//
//  BackBarButtonItem.m
//  Disrupt
//
//  Created by Izad Che Muda on 20/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "BackBarButtonItem.h"

@implementation BackBarButtonItem

- (id)initWithViewController:(UIViewController *)viewController {
    self = [super init];
    
    if (self) {
        self.viewController = viewController;
        self.image = [UIImage imageNamed:@"Back"];
        self.style = UIBarButtonItemStylePlain;
        self.target = self;
        self.action = @selector(buttonTapped:);
    }
    
    return self;
}


- (void)buttonTapped:(UIBarButtonItem *)sender {
    [self.viewController.navigationController popViewControllerAnimated:YES];
}


+ (id)backBarButtonItemWithViewController:(UIViewController *)viewController {
    return [[self alloc] initWithViewController:viewController];
}

@end
