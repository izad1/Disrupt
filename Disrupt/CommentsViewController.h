//
//  CommentsViewController.h
//  Disrupt
//
//  Created by Izad Che Muda on 19/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewController.h"

@interface CommentsViewController : TableViewController <UIGestureRecognizerDelegate>

@property (nonatomic, strong) NSDictionary *story;
@property (nonatomic, strong) NSMutableArray *comments;
@property (nonatomic) BOOL commentsLoaded;

- (void)fetchComments;

@end
