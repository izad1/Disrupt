//
//  Comment.h
//  Disrupt
//
//  Created by Izad Che Muda on 21/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Item.h"

@interface Comment : Item

@end
