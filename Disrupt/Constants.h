//
//  Constants.h
//  Disrupt
//
//  Created by Izad Che Muda on 19/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HexColors.h"

#define kAppName @"Disrupt"
#define kTintColor [UIColor colorWithHexString:@"#ff6600"]
#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kScreenHeight [UIScreen mainScreen].bounds.size.height
#define kContentFrame CGRectMake(0, 0, kScreenWidth, kScreenHeight - kNavBarHeight)
#define kNavBarHeight 64
