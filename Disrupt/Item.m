//
//  Item.m
//  Disrupt
//
//  Created by Izad Che Muda on 21/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "Item.h"

@implementation Item

- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    
    if (self) {
        self.dictionary = dictionary;
    }
    
    return self;
}


- (NSInteger)itemID {
    NSNumber *itemID = [self.dictionary objectForKey:@"id"];
    return itemID.integerValue;
}


- (NSString *)type {
    return [self.dictionary objectForKey:@"type"];
}


- (NSString *)by {
    return [self.dictionary objectForKey:@"by"];
}


- (NSInteger)time {
    NSNumber *time = [self.dictionary objectForKey:@"time"];
    return time.integerValue;
}

@end
