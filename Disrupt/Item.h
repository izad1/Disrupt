//
//  Item.h
//  Disrupt
//
//  Created by Izad Che Muda on 21/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject

@property (nonatomic, strong) NSDictionary *dictionary;
@property (nonatomic, readonly) NSInteger itemID;
@property (nonatomic, readonly) NSString *type;
@property (nonatomic, readonly) NSString *by;
@property (nonatomic, readonly) NSString *text;
@property (nonatomic, readonly) NSInteger time;


- (id)initWithDictionary:(NSDictionary *)dictionary;

@end
