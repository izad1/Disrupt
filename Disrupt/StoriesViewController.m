//
//  StoriesViewController.m
//  Disrupt
//
//  Created by Izad Che Muda on 19/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "StoriesViewController.h"
#import "StoryTableViewCell.h"
#import "LoadMoreTableViewCell.h"
#import "StoryViewController.h"
#import "CommentsViewController.h"
#import "Constants.h"
#import "Story.h"


@implementation StoriesViewController

NSInteger const kStoriesPerPage = 15;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.loadedStoryCount = 0;
    self.title = kAppName.uppercaseString;
            
    [self.tableView registerNib:[UINib nibWithNibName:@"StoryTableViewCell" bundle:nil] forCellReuseIdentifier:@"story"];
    [self.tableView registerNib:[UINib nibWithNibName:@"LoadMoreTableViewCell" bundle:nil] forCellReuseIdentifier:@"loadMore"];
    self.tableView.estimatedRowHeight = 71;
    self.tableView.rowHeight = UITableViewAutomaticDimension;    
    
    [self fetchStoriesIDs];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.storiesLoaded) {        
        return self.loadedStoryCount + 1;
    }
    
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.loadedStoryCount) {
        LoadMoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loadMore" forIndexPath:indexPath];
        [cell.button addTarget:self action:@selector(loadMoreButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        if (!self.isLoadingMoreStories) {
            cell.enabled = YES;
        }
        
        return cell;
    }
    else {
        StoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"story" forIndexPath:indexPath];
        [cell configureWithStory:[self.stories objectAtIndex:indexPath.row]];
        cell.commentButton.tag = indexPath.row;
        
        [cell.commentButton setHighlightBlock:^(id sender) {
            cell.commentButtonContainerView.backgroundColor = [UIColor colorWithHexString:@"#F6F6EF"];
        } dehighlightBlock:^(id sender) {
            cell.commentButtonContainerView.backgroundColor = [UIColor colorWithHexString:@"#FCFCFC"];
        }];
        
        [cell.commentButton addTarget:self action:@selector(commentButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.loadedStoryCount) {
        StoryViewController *storyVC = [StoryViewController new];
        storyVC.story = [self.stories objectAtIndex:indexPath.row];
        
        [self.navigationController pushViewController:storyVC animated:YES];
    }
}


- (void)refresh {
    [self fetchStoriesIDs];
}


- (void)fetchStoriesIDs {
    Firebase *ref = [[Firebase alloc] initWithUrl:@"https://hacker-news.firebaseio.com/v0/topstories"];
    
    [ref observeSingleEventOfType:FEventTypeValue
                        withBlock:^(FDataSnapshot *snapshot) {
                            self.storyIDs = snapshot.value;
                            self.placeholderStories = [NSMutableArray new];
                            
                            [self.storyIDs enumerateObjectsUsingBlock:^(NSNumber *storyID, NSUInteger idx, BOOL *stop) {
                                [self.placeholderStories addObject:[NSNull null]];
                            }];
                            
                            self.loadedStoryCount = 0;
                            [self fetchStoriesStartingFromIndex:0];
                        } withCancelBlock:^(NSError *error) {
                            [self.refreshControl endRefreshing];
                            [UIAlertController handleError:error withMessage:@"Failed to fetch stories." fromViewController:self];
                        }];
}


- (void)fetchStoriesStartingFromIndex:(NSInteger)index {
    NSInteger previousLoadedStoryCount = self.loadedStoryCount;
    self.loadedStoryCount = index + kStoriesPerPage;
    
    [[self.storyIDs subarrayWithRange:NSMakeRange(index, kStoriesPerPage)] enumerateObjectsUsingBlock:^(NSNumber *storyID, NSUInteger idx, BOOL *stop) {
        Firebase *ref = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacker-news.firebaseio.com/v0/item/%ld", storyID.integerValue]];
        
        [ref observeSingleEventOfType:FEventTypeValue
                            withBlock:^(FDataSnapshot *snapshot) {
                                Story *story = [[Story alloc] initWithDictionary:snapshot.value];
                                [self.placeholderStories replaceObjectAtIndex:idx + previousLoadedStoryCount withObject:story];
                                
                                if (self.storiesLoaded) {
                                    self.isLoadingMoreStories = NO;
                                    self.stories = [NSMutableArray arrayWithArray:self.placeholderStories];
                                    [self.refreshControl endRefreshing];
                                    [self.tableView reloadData];
                                }
                            } withCancelBlock:^(NSError *error) {
                                [self.refreshControl endRefreshing];
                                [UIAlertController handleError:error withMessage:@"Failed to fetch stories." fromViewController:self];
                            }];
    }];
}


- (BOOL)storiesLoaded {
    if (!self.placeholderStories.count) {
        return NO;
    }
    
    for (int idx = 0; idx < self.loadedStoryCount; idx++) {
        id story = [self.placeholderStories objectAtIndex:idx];
        
        if ([story isKindOfClass:[NSNull class]]) {
            return NO;
        }
    }
    
    return YES;
}


- (void)commentButtonTapped:(UIButton *)sender {
    /*
    CommentsViewController *commentsVC = [CommentsViewController new];
    commentsVC.story = [self.stories objectAtIndex:sender.tag];
    
    [self.navigationController pushViewController:commentsVC animated:YES];
     */
}


- (void)loadMoreButtonTapped:(UIButton *)sender {
    self.isLoadingMoreStories = YES;
    
    LoadMoreTableViewCell *cell = (LoadMoreTableViewCell *)sender.superview.superview.superview;
    cell.enabled = NO;
    
    [self fetchStoriesStartingFromIndex:self.loadedStoryCount];
}

@end
