//
//  LoadMoreTableViewCell.m
//  Disrupt
//
//  Created by Izad Che Muda on 21/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "LoadMoreTableViewCell.h"

@implementation LoadMoreTableViewCell

@synthesize enabled = _enabled;

- (void)awakeFromNib {
    // Initialization code
    
    self.containerView.layer.cornerRadius = 16;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


- (void)setEnabled:(BOOL)enabled {
    _enabled = enabled;
    
    if (_enabled) {
        self.containerView.alpha = 1;
        self.button.enabled = YES;
    }
    else {
        self.button.enabled = NO;
        
        [UIView animateWithDuration:0.25 animations:^{
           self.containerView.alpha = 0.2;
        }];
    }
}


- (BOOL)enabled {
    return _enabled;
}

@end
