//
//  UIAlertController+Error.m
//  Disrupt
//
//  Created by Izad Che Muda on 19/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "UIAlertController+Error.h"

@implementation UIAlertController (Error)

+ (void)handleError:(NSError *)error withMessage:(NSString *)message fromViewController:(UIViewController *)viewController {
    NSLog(@"%@", error);
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error"
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:nil]];
    
    [viewController presentViewController:alertController animated:YES completion:nil];
}

@end
