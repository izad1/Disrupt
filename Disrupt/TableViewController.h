//
//  TableViewController.h
//  Disrupt
//
//  Created by Izad Che Muda on 19/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>
#import "HexColors.h"
#import "UIAlertController+Error.h"


@interface TableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

- (void)refresh;

@end
