//
//  UIAlertController+Error.h
//  Disrupt
//
//  Created by Izad Che Muda on 19/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


@interface UIAlertController (Error)

+ (void)handleError:(NSError *)error withMessage:(NSString *)message fromViewController:(UIViewController *)viewController;

@end