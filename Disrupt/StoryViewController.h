//
//  StoryViewController.h
//  Disrupt
//
//  Created by Izad Che Muda on 20/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "Story.h"

@interface StoryViewController : UIViewController <UIGestureRecognizerDelegate>

@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, strong) Story *story;
@property (nonatomic, strong) UIView *progressView;

@end
