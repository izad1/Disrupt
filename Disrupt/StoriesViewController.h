//
//  StoriesViewController.h
//  Disrupt
//
//  Created by Izad Che Muda on 19/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewController.h"

@interface StoriesViewController : TableViewController

@property (nonatomic, strong) NSArray *storyIDs;
@property (nonatomic, strong) NSMutableArray *stories;
@property (nonatomic, strong) NSMutableArray *placeholderStories;
@property (nonatomic) BOOL storiesLoaded;
@property (nonatomic) BOOL isLoadingMoreStories;
@property (nonatomic) NSInteger loadedStoryCount;

- (void)fetchStoriesIDs;
- (void)fetchStoriesStartingFromIndex:(NSInteger)index;
- (void)commentButtonTapped:(UIButton *)sender;
- (void)loadMoreButtonTapped:(UIButton *)sender;

@end

