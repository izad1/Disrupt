//
//  ProxyButton.m
//  iMudah
//
//  Created by Izad Che Muda on 10/16/15.
//  Copyright © 2015 Elite Mobile Global Sdn Bhd. All rights reserved.
//

#import "ProxyButton.h"

@implementation ProxyButton

- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    
    if (self.highlightBlock && self.dehighlightBlock) {
        if (highlighted) {
            self.highlightBlock(self);
        }
        else {
            self.dehighlightBlock(self);
        }
    }
}

- (void)setHighlightBlock:(ProxyButtonBlock)highlightBlock dehighlightBlock:(ProxyButtonBlock)dehighlightBlock {
    self.highlightBlock = highlightBlock;
    self.dehighlightBlock = dehighlightBlock;
}

@end
