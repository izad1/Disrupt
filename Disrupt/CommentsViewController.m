//
//  CommentsViewController.m
//  Disrupt
//
//  Created by Izad Che Muda on 19/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "CommentsViewController.h"
#import "CommentTableViewCell.h"
#import "BackBarButtonItem.h"


@implementation CommentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationItem.leftBarButtonItem = [BackBarButtonItem backBarButtonItemWithViewController:self];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CommentTableViewCell" bundle:nil] forCellReuseIdentifier:@"comment"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.estimatedRowHeight = 97;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.comments = [[NSMutableArray alloc] initWithArray:[self.story objectForKey:@"kids"]];
    
    [self fetchComments];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (self.navigationController.viewControllers.count > 1) {
        return YES;
    }
    
    return NO;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.commentsLoaded) {
        return self.comments.count;
    }
    
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"comment" forIndexPath:indexPath];
    [cell configureWithComment:[self.comments objectAtIndex:indexPath.row]];
    
    return cell;
}


- (BOOL)commentsLoaded {
    if (!self.comments) {
        return NO;
    }
    
    for (int idx = 0; idx < self.comments.count; idx++) {
        id comment = [self.comments objectAtIndex:idx];
        
        if ([comment isKindOfClass:[NSNumber class]]) {
            return NO;
        }
    }
    
    return YES;
}


- (void)refresh {
    [self fetchComments];
}


- (void)fetchComments {
    [self.comments enumerateObjectsUsingBlock:^(id comment, NSUInteger idx, BOOL *stop) {
        if ([comment isKindOfClass:[NSNumber class]]) {
            NSNumber *commentID = comment;
            
            Firebase *ref = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacker-news.firebaseio.com/v0/item/%ld", commentID.integerValue]];
            
            [ref observeSingleEventOfType:FEventTypeValue
                                withBlock:^(FDataSnapshot *snapshot) {
                                    [self.comments replaceObjectAtIndex:idx withObject:snapshot.value];
                                    
                                    if (self.commentsLoaded) {
                                        [self.refreshControl endRefreshing];
                                        [self.tableView reloadData];
                                    }
                                } withCancelBlock:^(NSError *error) {
                                    [self.refreshControl endRefreshing];
                                    [UIAlertController handleError:error withMessage:@"Failed to fetch comments." fromViewController:self];
                                }];
        }
    }];
}


@end
