//
//  CommentTableViewCell.m
//  Disrupt
//
//  Created by Izad Che Muda on 19/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "CommentTableViewCell.h"
#import "NSDate+DateTools.h"
#import "Constants.h"
#import "NSString+HTML.h"


@implementation CommentTableViewCell

- (void)awakeFromNib {
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


- (void)configureWithComment:(NSDictionary *)comment {
    NSArray *kids = [comment objectForKey:@"kids"];
    
    if (kids) {
        self.contentLabelBottomMargin.constant = 45;
        self.button.hidden = NO;
        [self.button setTitle:[NSString stringWithFormat:@"load %ld repl%@", kids.count, kids.count > 1 ? @"ies" : @"y"]
                     forState:UIControlStateNormal];
    }
    else {
        self.contentLabelBottomMargin.constant = 16;
        self.button.hidden = YES;       
    }
    
    NSNumber *time = [comment objectForKey:@"time"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time.integerValue];
    self.agoLabel.text = date.timeAgoSinceNow;
    
    if ([comment objectForKey:@"deleted"]) {
        self.byLabel.text = @"[deleted]";
        self.contentLabel.text = @"[deleted]";
    }
    else {
        self.byLabel.text = [comment objectForKey:@"by"];
        NSString *text = [comment objectForKey:@"text"];
        self.contentLabel.text = [text stringByConvertingHTMLToPlainText];
    }
}

@end
